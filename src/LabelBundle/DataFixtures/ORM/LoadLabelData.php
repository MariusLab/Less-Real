<?php

namespace LabelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LabelBundle\Entity\Label;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadLabelData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function dataProvider(): array
    {
        $user = $this->container->get('user.provider')->loadUserByUsername('admin');

        return [
            [
                'One Piece',
                Label::LABEL_TYPE_ANIME,
                $user,
                true,
                1
            ],
            [
                'Death Note',
                Label::LABEL_TYPE_ANIME,
                $user,
                true,
                1
            ],
            [
                'Uzumaki Naruto',
                Label::LABEL_TYPE_CHARACTER,
                $user,
                true,
                1
            ]
        ];
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->dataProvider() as $data) {
            $label = (new Label())
                ->setText($data[0])
                ->setType($data[1])
                ->setUser($data[2])
                ->setVerified($data[3])
                ->setTotalEntitiesTagged($data[4])
            ;
            $manager->persist($label);
        }

        $manager->flush();
    }
}
