<?php

namespace LabelBundle\Service;

interface LabelerInterface
{
    public function tagEntity(int $labelId, int $entityId): void;

    public function untagEntity(int $labelId, int $entityId): void;

    public function isEntityTaggedWithLabel(int $labelId, int $entityId): bool;
}