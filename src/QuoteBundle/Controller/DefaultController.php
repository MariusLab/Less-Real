<?php

namespace QuoteBundle\Controller;

use GuzzleHttp\Client;
use restClient\Controller\RestClientController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use QuoteBundle\Entity\Quote;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $client = new RestClientController('http://localhost');
        $quote = $client->getQuote(1);

        return $this->render('QuoteBundle:Api:get_single_quote.html.twig', [
            'quoteBody' => $quote->getBody()
        ]);
    }

    public function getSingleQuoteAction()
    {
        $client = new RestClientController('http://localhost');
        $quote = $client->getQuote(1);

        return new Response($quote->getBody());

       /* $quote = $this->getDoctrine()
            ->getRepository('LRQuoteBundle:Quote')
            ->find(1);

        var_dump($quote);
       return new Response('s');*/
    }
}
