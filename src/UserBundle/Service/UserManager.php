<?php

namespace UserBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UserBundle\Controller\RegisterController;
use UserBundle\Entity\User;
use UserBundle\Exception\UserRegisterException;

class UserManager
{
    private $entityManager;
    private $passwordEncoder;
    private $validator;
    private $userProvider;
    private $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        ValidatorInterface $validator,
        UserProvider $userProvider,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->validator = $validator;
        $this->userProvider = $userProvider;
        $this->logger = $logger;
    }

    public function registerWithFacebook(User $user)
    {
        $this->register($user);
    }

    public function registerWithForm(User $user)
    {
        if ($user->getPlainPassword() === null || $user->getPlainPassword() === '') {
            throw new UserRegisterException(RegisterController::ERROR_MESSAGE_PASSWORD_EMPTY);
        }

        $this->register($user);
    }

    private function register(User $user)
    {
        $now = new \DateTime();
        $user
            ->setDateCreated($now)
            ->setDateUpdated($now)
        ;

        if ($user->getPlainPassword() !== null) {
            $user->setPassword(
                $this->passwordEncoder->encodePassword($user, $user->getPlainPassword())
            );
        } else {
            $user->setPassword(null);
        }

        $this->validate($user);

        $this->entityManager->persist($user);
    }

    /**
     * @param User $user
     * @throws UserRegisterException
     */
    private function validate(User $user)
    {
        if ($this->userProvider->loadUserByUsername($user->getUsername()) !== null) {
            throw new UserRegisterException(RegisterController::ERROR_MESSAGE_USERNAME_ALREADY_REGISTERED);
        }

        if ($this->userProvider->loadUserByEmail($user->getEmail()) !== null) {
            throw new UserRegisterException(RegisterController::ERROR_MESSAGE_EMAIL_ALREADY_REGISTERED);
        }

        $errors = $this->validator->validate($user);

        if (count($errors) > 0) {
            throw new UserRegisterException($errors[0]->getMessage());
        }
    }
}
