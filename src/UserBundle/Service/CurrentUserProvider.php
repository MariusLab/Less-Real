<?php

namespace UserBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Entity\User;

class CurrentUserProvider
{
    private $entityManager;
    private $tokenStorage;
    private $authorizationChecker;

    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED');
    }

    /**
     * @return User
     */
    public function getCurrent()
    {
        if ($this->isLoggedIn()) {
            return $this->tokenStorage->getToken()->getUser();
        } else {
            return null;
        }
    }
}
