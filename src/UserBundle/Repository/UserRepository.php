<?php

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\User;

class UserRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return null|User
     */
    public function findOne(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * @param int $id
     * @return null|User
     */
    public function findOneById(int $id)
    {
        return $this->find($id);
    }
}
