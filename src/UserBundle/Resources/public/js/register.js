$(document).ready(function() {
    //$('#form_username').focus();

    $('#register_form')
        .form({
            inline: true,
            on: 'change',
            delay: true,
            onSuccess : function(e) {
                e.preventDefault();
            },
            onInvalid: function() {
                $('#full_modal').modal('refresh').css('marginTop', '0');
            },
            fields: {
                username: {
                    identifier  : 'form[username]',
                    rules: [
                        {
                            type   : 'minLength[3]',
                            prompt : 'Make it at least 3 characters. You can do it!' + helpyFace
                        },
                        {
                            type   : 'maxLength[24]',
                            prompt : 'Woah there, slow down! This is the username field, not the bio!' + helpyFace
                        }
                    ]
                },
                email: {
                    identifier  : 'form[email]',
                    rules: [
                        {
                            type   : 'email',
                            prompt : 'I don\'t think this is an email address.' + helpyFace
                        }
                    ]
                },
                password: {
                    identifier  : 'form[password]',
                    rules: [
                        {
                            type   : 'minLength[6]',
                            prompt : 'I think to be secure, we need at least 6 characters!' + helpyFace
                        },
                        {
                            type   : 'maxLength[64]',
                            prompt : 'I don\'t think I\'ll be able to store this much!' + helpyFace
                        }
                    ]
                }
            }
        })
    ;

    var fields = [
        'username',
        'email',
        'password'
    ];

    formVisualResponse.init('register', fields);

    $('#register_submit').click(function(e) {
        modalLoading(true);

        var username = $('#register_username').val();
        var password = $('#register_password').val();
        var email = $('#register_email').val();
        $.ajax({
            method: 'POST',
            data: {
                username: username,
                password: password,
                email: email
            },
            url: uri("register_check"),
            success: function (response) {
                console.log(response);
                window.location = uri();
            },
            statusCode: {
                400: function (response) {
                    console.log(response);
                    $("#register_form_errors").hide().text('');
                    if (response.responseText.length > 0) {
                        $("#register_form_errors").show().text(response.responseText);
                    }
                    modalLoading(false);
                }
            },
            error: function (e) {

            }
        });
        e.preventDefault();
    });
});