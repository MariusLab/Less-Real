<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function dataProvider(): array
    {
        return [
            [
                'admin',
                'admin',
                'admin@localhost.com',
                'lessreal city'
            ]
        ];
    }

    public function load(ObjectManager $manager)
    {
        $passwordEncoder = $this->container->get('security.password_encoder');

        foreach ($this->dataProvider() as $data) {
            $user = (new User())
                ->setUsername($data[0])
                ->setPlainPassword($data[1])
                ->setEmail($data[2])
                ->setLocation($data[3])
            ;
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $user->getPlainPassword())
            );
            $manager->persist($user);
        }

        $manager->flush();
    }
}
