<?php

namespace UserBundle\Controller;

use AppBundle\Service\ImageManipulator;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UserBundle\Service\CurrentUserProvider;

class SettingsController extends Controller
{
    private $entityManager;
    private $logger;
    private $twig;
    private $formFactory;
    private $currentUserProvider;
    private $imageManipulator;
    private $currentUser;
    private $router;
    private $validator;

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        TwigEngine $twig,
        FormFactoryInterface $formFactory,
        CurrentUserProvider $currentUserProvider,
        ImageManipulator $imageManipulator,
        RouterInterface $router,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->currentUserProvider = $currentUserProvider;
        $this->imageManipulator = $imageManipulator;
        $this->currentUser = $this->currentUserProvider->getCurrent();
        $this->router = $router;
        $this->validator = $validator;
    }

    public function front(Request $request)
    {
        $form = $this->formFactory->createBuilder(FormType::class, null, [
            'attr' => [
                'id' => 'form'
            ]
        ])
            /*->add('username', TextType::class, [
                'attr' => [
                    'disabled' => '',
                    'placeholder' => $this->currentUser->getUsername()
                ]
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'disabled' => '',
                    'placeholder' => $this->currentUser->getEmail()
                ]
            ])*/
            ->add('gender', HiddenType::class, [
                'data' => $this->currentUser->getGender()
            ])
            ->add('cover', FileType::class, [
                'required' => false,
                'label' => 'Cover'
            ])
            ->add('cover_changed', HiddenType::class, [
                'data' => 0
            ])
            ->add('cover_crop_values', HiddenType::class, [
                'data' => 0
            ])
            /*->add('cover_change_button', ButtonType::class, [
                'label' => 'Change Cover'
            ])*/
            ->add('avatar', FileType::class, [
                'required' => false,
                'label' => 'Avatar'
            ])
            ->add('avatar_changed', HiddenType::class, [
                'data' => 0
            ])
            ->add('avatar_crop_values', HiddenType::class, [
                'data' => 0
            ])
            /*->add('avatar_change_button', ButtonType::class, [
                'label' => 'Change Avatar'
            ])*/
            ->add('location', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Your Location',
                    'autocomplete' => 'off',
                    'value' => $this->currentUser->getLocation()
                ]
            ])
            ->add('birthday', BirthdayType::class, [
                'data' => $this->currentUser->getBirthday()
            ])
            ->add('introduction', TextareaType::class, [
                'required' => false,
                'data' => $this->currentUser->getIntroduction()
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save Changes'
            ])
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if (!$form->isValid()) {
                return new Response('', 403);
            }

            $this->currentUser
                ->setIntroduction($form->get('introduction')->getData())
                ->setLocation($form->get('location')->getData())
                ->setBirthday($form->get('birthday')->getData())
                ->setGender($form->get('gender')->getData())
            ;

            if ($form->get('avatar_changed')->getData() == 1) {
                $this->logger->debug('Avatar change was requested');
                if (file_exists($this->currentUser->getRealNewAvatarPath())) {
                    $this->logger->debug('New avatar found.');
                    if (file_exists($this->currentUser->getRealCurrentAvatarPath())) {
                        $this->logger->debug('Current avatar found, deleting.');
                        unlink($this->currentUser->getRealCurrentAvatarPath());
                    }
                    $this->logger->debug('Moving new avatar to current avatar.');
                    if (rename(
                        $this->currentUser->getRealNewAvatarPath(),
                        $this->currentUser->getRealCurrentAvatarPath()
                    )) {
                        $this->currentUser->setDateAvatarChanged(new \DateTime());
                    }
                }
            }

            if ($form->get('cover_changed')->getData() == 1) {
                $this->logger->debug('Cover change was requested');
                if (file_exists($this->currentUser->getRealNewCoverPath())) {
                    $this->logger->debug('New cover found.');
                    if (file_exists($this->currentUser->getRealCurrentCoverPath())) {
                        $this->logger->debug('Current cover found, deleting.');
                        unlink($this->currentUser->getRealCurrentCoverPath());
                    }
                    $this->logger->debug('Moving new cover to current cover.');
                    if (rename(
                        $this->currentUser->getRealNewCoverPath(),
                        $this->currentUser->getRealCurrentCoverPath()
                    )) {
                        $this->currentUser->setDateCoverChanged(new \DateTime());
                    }
                }
            }

            $errors = $this->validator->validate($this->currentUser);
            if (count($errors) > 0) {
                $errorString = (string) $errors;
                $this->logger->debug('Form validation errors: ' . $errorString);
                return new Response('DATA: ' . $form->get('location')->getData(), 400);
            }
            $this->entityManager->persist($this->currentUser);
            $this->entityManager->flush();

            return new Response('', 200);
        }

        return $this->twig->renderResponse('UserBundle:Settings:settings.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function cover(Request $request)
    {
        if ($request->isMethod('GET')) {
            return $this->twig->renderResponse('UserBundle:Settings:cover.html.twig', [
            ]);
        } else {
            /** @var File $file */
            $file = $request->files->get('file');
            $extension = $file->guessExtension();

            $this->logger->info('Uploading cover...', ['file_real_path' => $file->getRealPath()]);

            if ($extension === "png") {
                $image = $this->imageManipulator->convertPngToJpg($file->getRealPath());
            } else {
                if (!$image = imagecreatefromjpeg($file->getRealPath())) {
                    throw new \Exception('Image is not of valid format.');
                }
            }

            $width = imagesx($image);
            $height = imagesy($image);
            $cropArea = $request->request->get('cropValues');
            $cropArea = explode(",", $cropArea);

            $widthRatio = $width/$request->request->get('imgWidth');
            $heightRatio = $height/$request->request->get('imgHeight');

            $x1 = floor($cropArea[0] * $widthRatio);
            $y1 = floor($cropArea[1] * $heightRatio);
            $x2 = floor($cropArea[2] * $widthRatio);
            $y2 = floor($cropArea[3] * $heightRatio);
            $nWidth = $x2 - $x1;
            $nHeight = $y2 - $y1;

            $croppedImage = $this->imageManipulator->cropImageAdvanced($image, $nWidth, $nHeight, $x1, $y1, $x2, $y2);
            $resizedImage = $this->imageManipulator->resizeImage($croppedImage, $nWidth, $nHeight, 1170, 300);
            imagejpeg($resizedImage, $this->currentUser->getRealNewCoverPath(), 100);
            if (get_resource_type($croppedImage) == "gd") {
                imagedestroy($croppedImage);
            }
            if (get_resource_type($resizedImage) == "gd") {
                imagedestroy($resizedImage);
            }

            return new Response($this->currentUser->getNewCoverPath(), 200);
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function avatar(Request $request)
    {
        if ($request->isMethod('GET')) {
            return $this->twig->renderResponse('UserBundle:Settings:avatar.html.twig', [
            ]);
        } else {
            /** @var File $file */
            $file = $request->files->get('file');
            $extension = $file->guessExtension();

            if ($extension === "png") {
                $image = $this->imageManipulator->convertPngToJpg($file->getRealPath());
            } else {
                if (!$image = imagecreatefromjpeg($file->getRealPath())) {
                    throw new \Exception('Image is not of valid format.');
                }
            }

            $width = imagesx($image);
            $height = imagesy($image);
            $cropArea = $request->request->get('cropValues');
            $cropArea = explode(",", $cropArea);

            $widthRatio = $width/$request->request->get('imgWidth');
            $heightRatio = $height/$request->request->get('imgHeight');

            $x1 = floor($cropArea[0] * $widthRatio);
            $y1 = floor($cropArea[1] * $heightRatio);
            $x2 = floor($cropArea[2] * $widthRatio);
            $y2 = floor($cropArea[3] * $heightRatio);
            $nWidth = $x2 - $x1;
            $nHeight = $y2 - $y1;

            $croppedImage = $this->imageManipulator->cropImageAdvanced($image, $nWidth, $nHeight, $x1, $y1, $x2, $y2);
            $resizedImage = $this->imageManipulator->resizeImage($croppedImage, $nWidth, $nHeight, 230, 230);
            imagejpeg($resizedImage, $this->currentUser->getRealNewAvatarPath(), 100);
            if (get_resource_type($croppedImage) == "gd") {
                imagedestroy($croppedImage);
            }
            if (get_resource_type($resizedImage) == "gd") {
                imagedestroy($resizedImage);
            }

            return new Response($this->currentUser->getNewAvatarPath(), 200);
        }
    }
}
