<?php

namespace ImageBundle\Controller;

use Gelf\MessagePublisher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class FrontController extends Controller
{
    public function indexAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)
            //echo "Hello, " . $this->getUser()->getUsername() . "!";
        } else {

        }
      /*  $images = $this->getDoctrine()->getRepository('ImageBundle:Images')
            ->getUntaggedUnfinishedImageUploads(1);

        foreach ($images as $image) {
            var_dump($image);
        }*/

        $imageApi = $this->get('image.api');
        $images = $this->getDoctrine()->getRepository('ImageBundle:Image')
            ->getNewestImages(30);

        $logger = $this->get('logger');
        $logger->info('Front page viewed');

        return $this->render('ImageBundle:Default:index.html.twig', [
            'images' => $images
        ]);
    }

    /*private function test(string $hello)
    {
        echo $hello;
    }*/

    public function uploadAction(Request $request)
    {
        if ($request->getMethod() == 'GET') {
            /*$em = $this->getDoctrine()->getManager();
            $image = $em->getRepository('ImageBundle:Image')->getLastEntity();

            echo "ID: " . $image->getId();
            echo "<br>HELLO: " . $image->getExtension();
            echo "<hr>";
            if (!$image) {
                throw new \Exception('Image not found.');
            }

            echo "<br>IMAGE NAME: " . $image->getPath();
            echo "<br>";
            echo "<img src='../../../web/images/".$image->getPath()."' style='max-width: 300px; max-height: 300px;' />";
            echo "<br>";*/

            //TODO: refactor code to work for all users
            $secondStep = 0;
            $unfinishedImageUploads = $this->getDoctrine()->getRepository('ImageBundle:Image')
                ->getUnfinishedImageUploads($this->get('user.provider.current')->getCurrent());
            if (count($unfinishedImageUploads) > 0) {
                $secondStep = 1;
            }

            return $this->render('ImageBundle:Default:upload.html.twig', array(
                'formSuccess' => 1,
                'secondStep' => $secondStep,
                'unfinishedImageUploads' => $unfinishedImageUploads
            ));
        }

        $em = $this->getDoctrine()->getManager();

        $image = new Image();
        $file = $request->files->get('file');
        $logger = $this->get('logger');
        $logger->debug('Filename: ' . $request->files->get('qqfile'));
        $image->setUserId(1);
        $image->setFile($request->files->get('qqfile'));
        $image->upload();
        $em->persist($image);
        $em->flush();
        $image->postUploadAndFlush();
        $em->flush();
        return new Response('', 404);

        /*$image->setUserId(1);
        $image->upload();
        $em->persist($image);
        $em->flush();
        $image->postUploadAndFlush();
        $em->flush();*/
    }

    public function uploadActionOld(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('ImageBundle:Image')->getLastEntity();

        echo "ID: " . $image->getId();
echo "<br>HELLO: " . $image->getExtension();
echo "<hr>";
        if (!$image) {
            throw new \Exception('Image not found.');
        }

        echo "<br>IMAGE NAME: " . $image->getFullname();
        echo "<br>";
        echo "<img src='../../../web/images/".$image->getFullname()."' style='max-width: 300px; max-height: 300px;' />";
        echo "<br>";

        $image = new Image();
        $form = $this->createFormBuilder($image)
            ->add('file', FileType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->getData();
            $image->setUserId(1);
            $image->upload();
            $em->persist($image);
            $em->flush();
            $image->postUploadAndFlush();
            $em->flush();

            return $this->redirectToRoute('fr_upload_image', ['success' => true]);
        }

        if ($request->query->get('success'))
            return $this->render('ImageBundle:Default:upload.html.twig', array(
                'formSuccess' => 1
            ));
        else
            return $this->render('ImageBundle:Default:upload.html.twig', array(
                'form' => $form->createView(),
                'formSuccess' => 0
            ));
    }

    public function getUnfinishedImageUploadsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $images = $em->getRepository('ImageBundle:Image')
            ->getUnfinishedImageUploads($this->get('user.provider.current')->getCurrent());

        return $this->render('ImageBundle:Default:unfinishedImageUploads.html.twig', array(
            'unfinishedImageUploads' => $images
        ));
    }

    public function getFinishedImageUploadsAction(Request $request)
    {
        $imageIds = $request->request->get('imageIds');
        $logger = $this->get('logger');
        $logger->debug('imageIds', ['imageIds' => $imageIds]);
        $em = $this->getDoctrine()->getManager();

        $images = $em->getRepository('ImageBundle:Image')
            ->findBy([
                'id' => $imageIds
            ])
        ;

        return $this->render('ImageBundle:Default:finishedImageUploads.html.twig', array(
            'finishedImageUploads' => $images
        ));
    }

    //TODO: put in front controller OR maybe call it ViewController
    public function getUnfinishedImageUploadsTagsAction(Request $request)
    {
        /** @var ApiController $imageApi */
        $imageApi = $this->get('image.api');
        $imageIds = $request->request->get('imageIds');
        $imageIds = json_decode($imageIds);

        $tags = $imageApi->getSharedImageTags($imageIds);

        return $this->render('LabelBundle:Default:entityLabelTags.html.twig', [
            'labels' => $tags
        ]);
    }

    public function getImageAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $image = $em->getRepository('ImageBundle:Image')
            ->find($id);

        if ($image === null) {
            throw new NotFoundResourceException('Image with such an id doesn\'t exist');
        }

        return $this->render('ImageBundle:Default:imageThumbnail.html.twig', array(
            'image' => $image
        ));
    }
}
