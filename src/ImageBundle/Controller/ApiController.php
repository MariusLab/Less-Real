<?php

namespace ImageBundle\Controller;

use AppBundle\Service\ImageManipulator;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use ImageBundle\Entity\Image;
use ImageBundle\Entity\ImageLabelTag;
use ImageBundle\Event\ImageUploadedEvent;
use ImageBundle\Repository\ImageRepository;
use ImageBundle\Repository\ImageLabelTagRepository;
use ImageBundle\Service\ImageLabeler;
use ImageBundle\Service\ImageManager;
use JMS\Serializer\Serializer;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Service\CurrentUserProvider;

class ApiController extends Controller
{
    private $entityManager;
    private $dispatcher;
    private $serializer;
    private $imageManipulator;
    private $currentUserProvider;
    private $imageRepository;
    private $labelTagRepository;
    private $imageLabeler;
    private $imageManager;
    private $logger;

    public function __construct(
        EntityManager $entityManager,
        EventDispatcherInterface $dispatcher,
        Serializer $serializer,
        ImageManipulator $imageManipulator,
        CurrentUserProvider $currentUserProvider,
        ImageRepository $imageRepository,
        ImageLabelTagRepository $labelTagRepository,
        ImageLabeler $imageLabeler,
        ImageManager $imageManager,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->dispatcher = $dispatcher;
        $this->serializer = $serializer;
        $this->imageManipulator = $imageManipulator;
        $this->currentUserProvider = $currentUserProvider;
        $this->imageRepository = $imageRepository;
        $this->labelTagRepository = $labelTagRepository;
        $this->imageLabeler = $imageLabeler;
        $this->imageManager = $imageManager;
        $this->logger = $logger;
    }

    private function validate(string $name): bool
    {
        $success = true;
        if ($name == 'upload') {
            if (count($this->imageRepository
                ->getUntaggedUnfinishedImageUploads($this->currentUserProvider->getCurrent(), 1)) == 1) {
                $success = false;
            }
        }

        return $success;
    }

    public function testExc()
    {
        $error = [
            [
                'type' => 'validation',
                'title' => 'A parameter was missing'
            ]
        ];
        return new Response(json_encode($error), 400);
    }

    public function finishUpload(): Response
    {
        if ($this->validate('upload')) {
            //TODO: make an image manager service
            $this->imageManager->finishImageUpload();
            $this->entityManager->flush();
            return new Response(json_encode(['success' => true]));
        } else {
            return new Response(json_encode(['success' => false]));
        }
    }

    public function getSingleImage($id)
    {
        $image = $this->imageRepository
            ->findOneBy(['id' => $id]);

        if (!$image)
        {
            return new Response('Image not found.');
        }

        $imageJson = $this->serializer->serialize($image, 'json');

        $response = new Response($imageJson, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function uploadImageOld(Request $request)
    {
        $imageData = json_decode($request->getContent(), true);

        //TODO: authenticate and thus get userId
        $userId = 1;

        $now = new \DateTime();
        $image = new Image();
        $image->setDateUpdated($now)
            ->setDateCreated($now)
            ->setUserId($userId)
            ->setName('test_image')
            ->setExtension('jpg');
        file_put_contents('images/test_image.jpg', base64_decode($imageData['image_str']));

        $em = $this->getDoctrine()->getManager();

        $em->persist($image);
        $em->flush();

        $response = new Response("<br>User: ".$userId, 200);

        return $response;
    }

    public function addTag(Request $request)
    {
        $info = $request->getContent();

        return new Response($info);
    }

    //TODO move to image entity
    private function generateUniqueHash()
    {
        return md5(random_bytes(10));
    }

    public function uploadImage(Request $request)
    {
        /** @var File $file */
        $file = $request->files->get('qqfile');

        list($imageWidth, $imageHeight) = getimagesize($file->getRealPath());
        $image = new Image();
        $image->setFileSize($file->getSize());
        $image->setFileExtension($file->guessExtension());
        $image->setWidth($imageWidth);
        $image->setHeight($imageHeight);

        $imageSource = null;
        //TODO: probably don't allow gifs
        if ($image->getFileExtension() == "jpg" || $image->getFileExtension() == "jpeg") {
            $imageSource = imagecreatefromjpeg($file->getRealPath());
        } elseif ($image->getFileExtension() == "gif") {
            $imageSource = imagecreatefromgif($file->getRealPath());
        } elseif ($image->getFileExtension() == "png") {
            $imageSource = imagecreatefrompng($file->getRealPath());
            $bg = imagecreatetruecolor(imagesx($imageSource), imagesy($imageSource));
            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
            imagealphablending($bg, true);
            imagecopy($bg, $imageSource, 0, 0, 0, 0, imagesx($imageSource), imagesy($imageSource));
            imagedestroy($imageSource);
            $quality = 100; // 0 = worst / smaller file, 100 = better / bigger file
            imagejpeg($bg, $file->getRealPath() . ".jpg", $quality);
            imagedestroy($bg);
            $imageSource = imagecreatefromjpeg($file->getRealPath() . ".jpg");
            $image->setFileExtension(Image::FILE_EXTENSION_JPG);
        }


        //$target2 = $_SERVER['DOCUMENT_ROOT']."/".globals::$uploadimagesthumbspath . $part . "/" . $filename;

        //TODO: generate filename
        $image->setFileName($this->generateUniqueHash() . '.' . $image->getFileExtension());
        $targetFull = 'images/preupload/fullImages/' . $image->getFileName();
        $targetFixedThumb = 'images/preupload/fixedThumbnails/' . $image->getFileName();
        $targetDynamicThumb = 'images/preupload/dynamicThumbnails/' . $image->getFileName();

        $fixedThumbImageSource = null;
        if ($image->getFileExtension() == "jpg" || $image->getFileExtension() == "jpeg") {
            imagejpeg($imageSource, $targetFull, 100);
            $fixedThumbImageSource = imagecreatefromjpeg($targetFull);
        } elseif ($image->getFileExtension() == "gif") {
            imagegif($imageSource, $targetFull);
            $fixedThumbImageSource = imagecreatefromgif($targetFull);
        } else {
            throw new \Exception('Oops! Something went wrong.');
        }

        //let's create a fixed size thumbnail
        $fixedThumbImageSource = $this->imageManipulator->cropImage(250, 180, $fixedThumbImageSource);

        if ($image->getFileExtension() == "jpg" || $image->getFileExtension() == "jpeg") {
            imagejpeg($fixedThumbImageSource, $targetFixedThumb, 100);
        } elseif ($image->getFileExtension() == "gif") {
            imagegif($fixedThumbImageSource, $targetFixedThumb);
        } else {
            throw new \Exception('Oops! Something went wrong.');
        }
        imagedestroy($fixedThumbImageSource);

        //let's create a dynamic thumbnail that adheres to the original aspect ratio
        $maxWidth = 230;
        $maxHeight = 230;

        $aspectRatio = $image->getWidth() / $image->getHeight();

        $newHeight = $maxHeight;
        $newWidth = round($maxHeight * $aspectRatio);

        $dynamicThumbSource = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($dynamicThumbSource, $imageSource, 0, 0, 0, 0, $newWidth, $newHeight, $imageWidth, $imageHeight);
        $image->setThumbnailWidth($newWidth);
        $image->setThumbnailHeight($newHeight);

        if ($image->getFileExtension() == "jpg" || $image->getFileExtension() == "jpeg") {
            imagejpeg($dynamicThumbSource, $targetDynamicThumb, 100);
        } elseif ($image->getFileExtension() == "gif") {
            imagegif($dynamicThumbSource, $targetDynamicThumb);
        } else {
            throw new \Exception('Oops! Something went wrong.');
        }
        imagedestroy($dynamicThumbSource);
        imagedestroy($imageSource);

        //TODO: set correct details
        $image->setDisplay(Image::DISPLAY_HORIZONTAL);
        $image->setHash('s');
        $image->setUser($this->currentUserProvider->getCurrent());
        $image->setStatus(Image::STATUS_UPLOADED);
        $this->entityManager->persist($image);
        $this->entityManager->flush();

        $this->dispatcher->dispatch(ImageUploadedEvent::NAME, new ImageUploadedEvent($image));

        return new Response(json_encode(['success' => true, 'uuid' => $image->getFileExtension()]), 200);
    }

    public function getSharedImageTags(array $imageIds): array
    {
        $this->logger->debug(__METHOD__);

        $imageTags = $this->labelTagRepository
            ->findBy([
                'image' => $imageIds
            ]);
        $totalImages = count($imageIds);
        $this->logger->debug('', ['total_images' => $totalImages]);

        $sharedImageTags = [];
        $imageTags2 = [];
        $imageTags2Count = [];
        /** @var ImageLabelTag $imageTag */
        foreach ($imageTags as $imageTag) {
            if (!isset($imageTags2[$imageTag->getLabel()->getId()])) {
                $imageTags2Count[$imageTag->getLabel()->getId()] = 1;
                $imageTags2[$imageTag->getLabel()->getId()] = $imageTag->getLabel();
            } else {
                $imageTags2Count[$imageTag->getLabel()->getId()]++;
            }
        }

        foreach ($imageTags2Count as $imageTagId => $count) {
            if ($count == $totalImages) {
                $sharedImageTags[] = $imageTags2[$imageTagId];
            }
        }

        return $sharedImageTags;
    }

    public function testUploadImage()
    {
        $client = new Client(['base_uri' => 'http://localhost']);

        $response = $client->get('http://www.less-real.com/imagevault/uploaded/images/part7/Smiling-31094.jpg');
        $image = $response->getBody()->getContents();

        $response = $client->request(
            'POST',
            'http://localhost/api/image',
            [
                'Content-Type' => 'application/json',
                'body' => json_encode([
                    "image_str" => base64_encode($image)
                    ])
            ]
        );

        if ($response->getStatusCode() === 200) {
            echo "<b>Done!</b>";
            echo $response->getBody()->getContents();
        } else {
            echo "<b>Oops! Something went wrong.</b>";
        }

        return new Response('');
    }

    public function updateImage(Request $request, $id)
    {
        $image = $this->serializer->deserialize($request->getContent(), Image::class, 'json');
        $image->setDateCreated(new \DateTime())
            ->setDateUpdated(new \DateTime());
        $this->entityManager->persist($image);
        $this->entityManager->flush();

        $response = new Response('', 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function testUpdateImage()
    {
        $client = new Client(['base_uri' => 'http://localhost']);

        $image = $this->imageRepository->find(1);

        echo $image->getName();
        echo "<br>";
        $image = $this->serializer->serialize($image, 'json');
        echo $image;
        die;
        $image = $this->serializer->serialize($image, 'json');
        echo $image;
        $image = $this->serializer->deserialize($image, Image::class, 'json');
        echo "<br><br>".$image->getDateCreated()."<br><br>";
        $image->setName('Something Else')
            ->setDateCreated(new \DateTime())
            ->setDateUpdated(new \DateTime());
        $response = $client->request(
            'PUT',
            'http://localhost/api/image/1',
            [
                'Content-Type' => 'application/json',
                'body' => $this->serializer->serialize($image, 'json')
            ]
        );

        if ($response->getStatusCode() === 200) {
            echo "<b>Done!</b>";
            echo $response->getBody()->getContents();
        } else {
            echo "<b>Oops! Something went wrong.</b>";
        }

        return new Response('');
    }

    public function tagImage(Request $request, int $labelId): Response
    {
        $imageIds = json_decode($request->request->get('imageIds'));

        foreach ($imageIds as $imageId) {
            if ($this->imageLabeler->isEntityTaggedWithLabel($labelId, $imageId) === true) {
                continue;
                //throw new \Exception('Object already tagged with this tag.');
            }
            $this->imageLabeler->tagEntity($labelId, $imageId);
        }

        $this->entityManager->flush();

        return new Response('', 201);
    }

    public function untagImage(Request $request, int $labelId): Response
    {
        $imageIds = json_decode($request->request->get('imageIds'));

        foreach ($imageIds as $imageId) {
            if ($this->imageLabeler->isEntityTaggedWithLabel($labelId, $imageId) === false) {
                continue;
                //throw new \Exception('Object already tagged with this tag.');
            }
            $this->imageLabeler->untagEntity($labelId, $imageId);
        }

        $this->entityManager->flush();

        return new Response('', 200);
    }
}
