var imageIds = [];

function finishUpload()
{
    $.ajax({
        url: uri('api/image/upload'),
        method: 'GET',
        success: function(response) {
            var res = JSON.parse(response);
            imageIds = getImageIds(imageIds);
            if (res.success == 1) {
                notify(NOTIFY_SUCCESS, 'Images added successfully.');
                switchToThirdStep(imageIds);
            } else {
                notify(NOTIFY_ERROR, 'Every image should have at least one tag.');
            }
        }
    });
}

function updateTags(objectType, objectIds, showLoader)
{
    if (showLoader === true || showLoader === undefined) {
        $('#tag_section_loader').addClass('active');
    }
    var tempObjectIds = getImageIds(objectIds);
    $('#image_upload_show_tags').load(uri('api/render/unfinishedImageUploadsTags'), {imageIds: JSON.stringify(tempObjectIds)}, function(response) {
        if (showLoader === true || showLoader === undefined) {
            $('#tag_section_loader').removeClass('active');
        }
        $('.label').on('click', function() {
            $tagDOM = $(this);
            var labelId = $tagDOM.attr('data-label-id');
            $tagDOM.remove();

            $.ajax({
                url: uri('api/image/untag/' + labelId),
                data: { imageIds: JSON.stringify(tempObjectIds) },
                method: 'DELETE',
                success: function(response, statusText, xhr) {
                    if (xhr.status !== 200) {
                        alert('Oops! Tag couldn\'t be removed.');
                    } else {

                    }
                }
            });
        }).on('mouseover', function() {
            $(this).removeClass('blue').addClass('red linethrough');
        }).on('mouseleave', function() {
            $(this).removeClass('red linethrough').addClass('blue');
        });
    });
}

function getImageIds(imageIds)
{
    var tempObjectIds = imageIds.slice();
    if (tempObjectIds.length == 0) {
        $('.uploaded').each(function(i, obj) {
            var imageId = $(this).attr('data-image-id');
            if (imageId != 0) {
                addToImageIds(tempObjectIds, imageId);
            }
        });
    }

    return tempObjectIds;
}

function addToImageIds(imageIds, imageId)
{
    var inArrayIndex = $.inArray(imageId, imageIds);
    if (inArrayIndex >= 0) {
        imageIds.splice(inArrayIndex, 1);
    } else {
        imageIds.push(imageId);
    }
}

function initSecondStep()
{
    updateTags('images', imageIds);
    $('.uploaded').on('click', function() {
        $('.uploaded').removeClass('selected');
        var imageId = $(this).attr('data-image-id');
        addToImageIds(imageIds, imageId);

        for (var i = 0; i < imageIds.length; i++) {
            $('[data-image-id=' + imageIds[i] + ']').addClass('selected');
        }

        updateTags('images', imageIds);
    });

    $('#tag_search').simplete();
}

function initThirdStep()
{

}

function switchToSecondStep(speed)
{
    $('#image_upload_unfinished_images').load('/api/render/unfinishedImageUploads', function(response) {
        if (speed == 'instant') {
            $('#image_upload_step_one').hide();
            $('#image_upload_step_two').show();
        } else {
            $('#image_upload_step_one').fadeOut();
            $('#image_upload_step_two').fadeIn();
        }

        initSecondStep();
    });
}

function switchToThirdStep(imageIds)
{
    $('#image_upload_finished_images').load('/api/render/finishedImageUploads', { imageIds: imageIds }, function(response) {
        $('#image_upload_step_two').fadeOut();
        $('#image_upload_step_three').fadeIn();
        initThirdStep();
    });
}

//TODO: make it parse all types of messages
function parseApiResponse(response)
{
    response = JSON.parse(response);
    if (response.length > 0 ) {
        notify(NOTIFY_ERROR, 'Something went wrong.');
    }

    return response;
}

$(document).ready(function() {
    if ($('#image_upload_step_two').is(':visible')) {
        initSecondStep();
    }

    $('.tag').on('click', function() {
       $.ajax({
           url: '/testexc',
           method: 'GET',
           success: function(response) {
               alert('Success response: ' + response);
           },
           error: function(xhr, textStatus, errorThrown) {
               var errors = parseErrorResponse(xhr.responseText);
           }
       });
    });
    socket.emit('image_uploaded');

    var uploader = new qq.FineUploaderBasic({
        debug: false,
        button: document.getElementById('image_upload_preview'),
        request: {
            endpoint: uri('api/image'),
            method: 'POST'
        },
        validation: {
            acceptFiles: 'image/jpeg, image/png',
            image: {
                minWidth: 300,
                minHeight: 300
            }
        },
        autoUpload: false,
        dragAndDrop: {
            extraDropzones: [ $("#image_upload_preview") ]
        },
        callbacks: {
            onSubmit: function(id, name) {
                $('#image_upload_preview').append("<div class='uploaded-image-wrapper padded-for-info'><div class='dimmer'></div><div class='info'>" + name + "</div><div class='remove'><i class='trash icon'></i></div><img /></div>");
                uploader.drawThumbnail(id, document.getElementById('image_upload_preview').lastChild.lastChild);
                $('#image_upload_preview').children().last().attr('data-image-id', id);

                $('.uploaded-image-wrapper').click(function() {
                    uploader.cancel(id);
                    $(this).remove();
                });
            },
            onComplete: function(id, name, responseJSON, xhr) {
                $('img').find("[data-image-id='" + id + "']").toggle('highlight');
            },
            onAllComplete: function(succeedIds, failIds) {
                switchToSecondStep();
            },
            onProgress: function(id, name, uploadedBytes, totalBytes) {
                console.log('Image id: ' + id);
                console.log('uploadedBytes: ' + uploadedBytes + ' / ' + totalBytes);
            }
        }
    });

    $('#image-upload-start').on('click', function() {
        if (uploader.getUploads({status: qq.status.SUBMITTED}).length > 0) {
            $(this).addClass('loading');
            uploader.uploadStoredFiles();
        }
    });

    $('#image_upload_finish').on('click', function() {
        finishUpload();
    });
});
