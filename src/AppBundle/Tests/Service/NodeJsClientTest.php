<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\NodeJsClient;
use ImageBundle\Entity\Image;
use ImageBundle\Event\ImageUploadedEvent;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NodeJsClientTest extends WebTestCase
{
    /**
     * @var NodeJsClient
     */
    private $nodeClient;

    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();


        $this->nodeClient = $kernel->getContainer()->get('app.service.nodejs_client');
    }

    public function testDispatchEvent()
    {
        /*$image = new Images();
        $image->setHashID('sad');
        $image->setUser('Ishimaru');

        $event = new ImageUploadedEvent($image);
        $response = $this->nodeClient->dispatchEvent(ImageUploadedEvent::NAME, $event);*/

        //$this->assertEquals(ImageUploadedEvent::NAME, $response->getBody()->getContents());
    }
}
