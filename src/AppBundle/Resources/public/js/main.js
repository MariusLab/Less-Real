var NOTIFY_SUCCESS = 'positive';
var NOTIFY_ERROR = 'error';
var NOTIFY_WARNING = 'warning';
var NOTIFY_INFO = 'info';
var NOTIFY_BLUE = 'blue';

var notifyTimeout = null;

var socket = io.connect('http://' + document.domain + ':8000');

socket.on('image.uploaded', function(data) {
    $.ajax({
        method: 'GET',
        url: uri('api/render/image/' + data.id),
        success: function(response) {
            var image = $(response);
            $('.images').prepend(image);
            image.hide().fadeIn('slow');
        }
    });
});

function loadJavascript(assetUrl) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = assetUrl;
    document.body.appendChild(script);
}

function notify(type, message) {
    clearTimeout(notifyTimeout);
    var notify = $('#notify');
    notify
        .empty()
        .html("<div class='ui message " + type + "'>" + message + "</div>").hide().css('opacity', 0).slideDown().animate({ opacity: 1 }, { queue: false, duration: 'normal' });
    notifyTimeout = setTimeout(function() {
        notify.slideUp().animate({ opacity: 0 }, { queue: false, duration: 'normal' });
    }, 5000);
}

function uri(url)
{
    var base_uri = '';
    if (url === undefined) {
        return base_uri;
    } else {
        return base_uri + '/' + url;
    }
}

function modalLoading(state) {
    if (state === true) {
        $('#full_modal').children('div').addClass('loading');
    } else {
        $('#full_modal').children('div').removeClass('loading');
    }
}

function modalShow(state) {
    if (state === true) {
        $('#full_modal').modal('show');
    } else {
        $('#full_modal').modal('hide');
    }
}

$(document).scroll(function() {
    if ($(this).scrollTop() > 20) {
        $('#navigation').addClass('scrolled');
    } else {
        $('#navigation').removeClass('scrolled');
    }
});

$(document).ready(function() {
    $('#notify').mouseenter(function() {
        $(this).fadeOut('fast');
    });
    
    $('.ui.dropdown')
        .dropdown({
            action: 'hide',
            transition: 'drop',
            showOnFocus: true
        })
    ;

    $('.ui.selection.dropdown')
        .dropdown({
            action: 'activate',
            transition: 'slide down'
        })
    ;
    $('#full_modal').modal({
        dimmerSettings: { opacity: 0.5 },
        transition: 'fade down'
    });

    $('#sign-in, .sign-in').click(function(e) {
        e.preventDefault();
        modalLoading(true);
        modalShow(true);
        $.ajax({
            url: uri('login'),
            method: 'GET',
            success: function (response) {
                //alert('Success response: ' + response);
                modalLoading(false);
                $('#full_modal').html(response).modal('refresh').css('marginTop', '0');
            },
            error: function (xhr, textStatus, errorThrown) {
                //var errors = parseErrorResponse(xhr.responseText);
            }
        });
    });

    $('#sign-up, .sign-up').click(function(e) {
        e.preventDefault();
        modalLoading(true);
        modalShow(true);
        $.ajax({
            url: uri('register'),
            method: 'GET',
            success: function (response) {
                //alert('Success response: ' + response);
                modalLoading(false);
                $('#full_modal').html(response).modal('refresh').css('marginTop', '0');
            },
            error: function (xhr, textStatus, errorThrown) {
                //var errors = parseErrorResponse(xhr.responseText);
            }
        });
    });
});
