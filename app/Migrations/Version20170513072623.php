<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170513072623 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, status VARCHAR(20) NOT NULL, source VARCHAR(200) DEFAULT NULL, hash VARCHAR(100) NOT NULL, file_name VARCHAR(200) NOT NULL, file_extension VARCHAR(10) NOT NULL, file_size INT NOT NULL, display INT NOT NULL, width INT NOT NULL, height INT NOT NULL, thumbnail_width INT NOT NULL, thumbnail_height INT NOT NULL, date_updated DATETIME NOT NULL, date_created DATETIME NOT NULL, UNIQUE INDEX UNIQ_C53D045FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE label (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, verified INT NOT NULL, type INT NOT NULL, text VARCHAR(512) NOT NULL, description LONGTEXT DEFAULT NULL, total_entities_tagged INT NOT NULL, date_updated DATETIME NOT NULL, date_created DATETIME NOT NULL, INDEX IDX_EA750E8A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE label ADD CONSTRAINT FK_EA750E8A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE objects_tags');
        $this->addSql('DROP TABLE primary_tags');
        $this->addSql('ALTER TABLE user DROP INDEX UNIQ_8D93D649F85E0677, ADD INDEX ix_username (username)');
        $this->addSql('ALTER TABLE user CHANGE password password VARCHAR(64) DEFAULT NULL, CHANGE date_avatar_changed date_avatar_changed DATETIME NOT NULL, CHANGE date_cover_changed date_cover_changed DATETIME NOT NULL');
        $this->addSql('CREATE INDEX ix_email ON user (email)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE images (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(20) DEFAULT NULL COLLATE utf8_unicode_ci, game INT DEFAULT NULL, source VARCHAR(512) DEFAULT NULL COLLATE utf8_unicode_ci, user VARCHAR(200) DEFAULT NULL COLLATE utf8_unicode_ci, hashID VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, image VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, imagethumb VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, lmtimestamp INT DEFAULT NULL, tag VARCHAR(200) DEFAULT NULL COLLATE utf8_unicode_ci, addtags VARCHAR(400) DEFAULT NULL COLLATE utf8_unicode_ci, display INT DEFAULT NULL, width INT NOT NULL, height INT NOT NULL, thumbwidth INT DEFAULT NULL, thumbheight INT DEFAULT NULL, type VARCHAR(10) DEFAULT NULL COLLATE utf8_unicode_ci, size INT DEFAULT NULL, comments INT DEFAULT NULL, favorites INT DEFAULT NULL, votesUp INT DEFAULT NULL, votesDown INT DEFAULT NULL, views INT DEFAULT NULL, report INT DEFAULT NULL, description VARCHAR(500) DEFAULT NULL COLLATE utf8_unicode_ci, timestamp INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE objects_tags (id INT AUTO_INCREMENT NOT NULL, tag_id INT NOT NULL, object_table VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, object_id INT NOT NULL, user_id INT NOT NULL, date_updated INT NOT NULL, date_created INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE primary_tags (id INT AUTO_INCREMENT NOT NULL, verified INT NOT NULL, type INT NOT NULL, label VARCHAR(512) NOT NULL COLLATE utf8_unicode_ci, description LONGTEXT NOT NULL COLLATE utf8_unicode_ci, objects_tagged INT NOT NULL, quote_tagged INT NOT NULL, user_id INT NOT NULL, date_updated INT NOT NULL, date_created INT NOT NULL, views INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE label');
        $this->addSql('ALTER TABLE user DROP INDEX ix_username, ADD UNIQUE INDEX UNIQ_8D93D649F85E0677 (username)');
        $this->addSql('DROP INDEX ix_email ON user');
        $this->addSql('ALTER TABLE user CHANGE password password VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, CHANGE date_avatar_changed date_avatar_changed DATETIME DEFAULT NULL, CHANGE date_cover_changed date_cover_changed DATETIME DEFAULT NULL');
    }
}
